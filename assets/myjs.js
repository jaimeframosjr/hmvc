var table;
$(document).ready(function() {
    $('#nossq').DataTable();
    $(document).on('click','#add', function(){
        $('#addrecord').modal('show');
    });
    //datatables
    table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        ajax: "data.json",
        "aoColumns": [
					    null,
					    { "sClass": "fname" },
					    { "sClass": "lname" },null
					], //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        // Load data for the table's content from an Ajax source
        "ajax": { 
            "url": "main/ajax_list",
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
        
 
    });

    $(document).on('click', '.dataview',function(){
    	console.log($(this).attr('data-id'));
    	
	  	$tr = $(this).closest('tr');
         var fname = $('.fname', $tr).text();
         var lname = $('.lname', $tr).text();    
		console.log(fname + lname);
		  $('#fname').val(fname);
          $('#lname').val(lname);
          $('#id').val($(this).attr('data-id'));
    	$('#view').modal('show');
    });
    //update record
     $(document).on('click', '#update',function(){
            $.ajax({
                type:'POST',
                url: "main/ajax_update",
                data: {"id": $('#id').val(), "fname": $('#fname').val(), "lname": $('#lname').val()},
                success: function(data){
                    alertify.set('notifier','position', 'top-right');
                    alertify.success('Record Updated!');
                    table.ajax.reload();
                }
            });
     });
     //add record
      $(document).on('click', '#save',function(){
        
            $.ajax({
                type:'POST',
                url: "main/ajax_add",
                data: {"fname": $('#myfname').val(), "lname": $('#mylname').val()},
                success: function(data){
                    alertify.set('notifier','position', 'top-right');
                    alertify.success('Record saved!');
                     table.ajax.reload();
                     console.log(data);
                }
            });
     });
      $(document).on('click','.delete',function(){
          var id = $(this).attr('data-id');
            alertify.confirm('Delete Record','Are you sure?', 
                function(){ 
                     $.ajax({
                            type:'POST',
                            url: "main/ajax_delete",
                            data: {"id":id},
                            success: function(data){
                                alertify.set('notifier','position', 'top-right');
                                alertify.success('Deleted!');
                                table.ajax.reload();
                                console.log(data);
                        }
                    });
                    
                }, 
                    function(){ 
                        alertify.set('notifier','position', 'top-right');
                        alertify.error('Cancel');
                    }
                );
            console.log(id);
      });


});