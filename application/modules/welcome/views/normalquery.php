<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">Codeigniter 3 (Hierarchical Model View Controller)<button type="button" class="btn btn-primary" id="add">Add Record</button></div>
				</div>
				<div class="panel-body">
					 <table id="nossq" class="table table-bordered">
						<thead>
							<tr>
								<th>Id</th>
								<th>First Name</th>
								<th>Last Name</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($test as $d ):?>
							<tr>
								<td><?=$d->id?></td>
								<td><?=$d->fname?></td>
								<td><?=$d->lname?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table> 
				</div>
			</div>
		</div>
	</div>
</div>