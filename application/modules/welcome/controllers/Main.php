<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	public function __construct()
       {
            parent::__construct();
            $this->load->model('Mainmodel' ,'mm');
            $this->load->model('serversidemodel' ,'ssm'); 

       }
	public function test()
	{
		 $id = $this->uri->segment(4);
		 $list = $this->mm->test($id);
		 echo json_encode($list);	
	}
	public function insert()
	{
		 $list = $this->mm->insert();
	}
	public function index(){
		
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function normalquery(){
		
		$data['test'] = $this->mm->user();
		$this->load->view('header');
		$this->load->view('normalquery', $data);
		$this->load->view('footer');
	}
	public function ajax_add(){
		
		$data['fname'] = $this->input->post('fname');
		$data['lname'] = $this->input->post('lname');
		
		
		$query = $this->mm->add($data);
	}
	public function ajax_delete(){
		
		$id = $this->input->post('id');
		$query = $this->mm->delete($id);
	}
	public function ajax_update(){
		$data['fname'] = $this->input->post('fname');
		$data['lname'] = $this->input->post('lname');
		$id = $this->input->post('id');
		
		$query = $this->mm->update($id,$data);

	}
	 public function ajax_list()
    {
        $list = $this->ssm->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $info) {
            $row = array();
            $row[] = $info->id;
            $row[] = $info->fname;
            $row[] = $info->lname;
            $row[] = '<button type="button" class="btn btn-success dataview" data-id="'.$info->id.'">Edit</button>
					  <button type="button" class="btn btn-danger delete" data-id="'.$info->id.'">Delete</button>
					  <button type="button" class="btn btn-primary axiosrecord" data-id="'.$info->id.'">Axios Request view</button>
						';
 
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->ssm->count_all(),
                        "recordsFiltered" => $this->ssm->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
}