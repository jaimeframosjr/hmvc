<?php defined('BASEPATH') OR exit('No direct script access allowed');
	class Mainmodel extends CI_Model {

        public function __construct()
        {
                parent::__construct();
                
        }
        public function user(){
        	 $query = $this->db->get('info')->result();
        	return $query;
        } 
         public function update($id,$data){
         	 $this->db->where('id', $id);
        	 $query = $this->db->update('info',$data);
        } 
        public function add($data){
         	 $this->db->insert('info', $data);
	}
	public function delete($id){
		 $this->db->where('id', $id);
         	 $this->db->delete('info');
	}
	public function test($id){
		$this->db->where('id', $id);
		$query = $this->db->get('info')->result();
                return $query;;
	}
        public function insert(){
                ini_set('max_execution_time', 300);
                 for($x=0;$x<10000;$x++){
                        $data = array('fname'=>$x, 'lname' => $x);
                       $this->db->insert('info', $data);
                 }
        }

} 